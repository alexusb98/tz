//
//  TZAppDelegate.h
//  TZ
//
//  Created by Alexey Yakushev on 4/9/14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Instagram.h"

@interface TZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) Instagram *instagram;
@property (strong, nonatomic) UINavigationController *navigationController;
@end
