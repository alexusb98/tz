//
//  UserObject.h
//  TZ
//
//  Created by Alexey Yakushev on 4/17/14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserObject : NSObject

@property(strong, nonatomic) NSString* userName;
@property(strong, nonatomic) NSNumber* userId;

@end
