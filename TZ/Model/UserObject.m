//
//  UserObject.m
//  TZ
//
//  Created by Alexey Yakushev on 4/17/14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import "UserObject.h"

@implementation UserObject

- (void)dealloc {
    self.userName = nil;
    self.userId = nil;
}

@end
