

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface InstagramObject : NSObject

@property(strong, nonatomic) NSString* imageLowResolutionURL;
@property(strong, nonatomic) NSString* imageStandardResolutionURL;
@property(strong, nonatomic) NSString* imageThumbnailURL;
@property(strong, nonatomic) NSString* text;
@property(strong, nonatomic) NSNumber* likesCount;

@end
