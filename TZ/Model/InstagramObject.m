

#import "InstagramObject.h"

@implementation InstagramObject

- (void)dealloc {
    _imageLowResolutionURL = nil;
    _imageStandardResolutionURL = nil;
    _imageThumbnailURL = nil;
    _text = nil;
    _likesCount = nil;
}

@end
