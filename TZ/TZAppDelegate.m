//
//  TZAppDelegate.m
//  TZ
//
//  Created by Alexey Yakushev on 4/9/14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

/*
 Тестовое задание:
 В программе при запуске есть одно поле для ввода и кнопка «Давай коллаж». Вводим ник пользователя из инстаграма, приложение скачивает лучшие фотографии пользователя, склеивает их в коллаж, показывает этот коллаж и кнопку «В печать», нажатие на которую отправляет фотографию на принтер. Язык — ObjC.
 Более простой вариант — печать одной лучшей фотографии.
 Более сложный вариант — сделать фото-пикер, позволяющий выбрать из лучших фотографий более лучшие и сделать коллаж из них.
 */
/*
 1. В приложении присутствует ошибка:
 при запросе фотографий пользователя, приложение запрашивает все фотографии (выполняется несколько запросов к серверу instagram), а отображает фотографии, полученные только в первом запросе.
 
 2. Очень запутанный код, сложный для понимая. Не наглядная иерархия вызова функций. Возможно комментарии и некоторая оптимизация исправит ситуацию.

 */
#import "TZAppDelegate.h"
#import "TZMainVC.h"
#import "Constants.h"

@implementation TZAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.instagram = [[Instagram alloc] initWithClientId:instagramClientId
                                                delegate:nil];
//    [self registerDefaults];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    TZMainVC *vc = [[TZMainVC alloc] initWithNibName:@"TZMainVC" bundle:nil] ;
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
    self.window.rootViewController = self.navigationController;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Instagram
-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    return [self.instagram handleOpenURL:url];
}
-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [self.instagram handleOpenURL:url];
}

@end
