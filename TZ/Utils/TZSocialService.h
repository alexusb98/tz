//
//  TZSocialService.h
//  TZ
//
//  Created by Alexey Yakushev on 4/14/14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Instagram.h"

@interface TZSocialService : NSObject <IGSessionDelegate, IGRequestDelegate>

#pragma mark - Instagram
- (void)picturesForUser:(NSString*)userName completion:(void(^)(void))completionBlock;

- (void)picturesForUserId:(NSNumber*)userId completion:(void(^)(void))completionBlock;
@end
