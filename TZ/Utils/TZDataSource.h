

#import <Foundation/Foundation.h>

@interface TZDataSource : NSObject

@property (strong, nonatomic) NSArray *dataInstagram;
@property (strong, nonatomic) NSArray *dataAmbiguousUsers;

- (id) init;
+ (id) sharedInstance;

@end
