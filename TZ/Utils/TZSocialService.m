//
//  TZSocialService.m
//  TZ
//
//  Created by Alexey Yakushev on 4/14/14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import "TZSocialService.h"
#import "TZAppDelegate.h"
#import "TZDataSource.h"
#import "Constants.h"
#import "InstagramObject.h"
#import "UserObject.h"

typedef void (^completion)(void);

@interface TZSocialService()

@property (strong, nonatomic) NSUserDefaults* defaults;
@property (strong, nonatomic) TZAppDelegate* appDelegate;
@property (strong, nonatomic) Instagram* instagram;
@property (strong, nonatomic) TZDataSource *dataSource;
@property (strong, nonatomic) NSMutableDictionary *instagramRequestURLs;
@property (strong, nonatomic) NSMutableArray *instagramImages;
@property (strong, nonatomic) NSString *urlString;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSMutableString *nextMaxId;
@property (readwrite, copy) completion completionBlock;
@property (strong, nonatomic) NSNumber *userId;

@end


@implementation TZSocialService
- (instancetype)init {
    self = [super init];
    if (self) {
        // Custom initialization

        self.appDelegate = (TZAppDelegate*)[UIApplication sharedApplication].delegate;
        self.defaults = [NSUserDefaults standardUserDefaults];
        self.instagram = self.appDelegate.instagram;
        self.instagram.sessionDelegate = self;
        self.dataSource = [TZDataSource sharedInstance];
        self.instagramRequestURLs = [[NSMutableDictionary alloc] init];
        self.instagramImages = [[NSMutableArray alloc] init];
        self.nextMaxId = [[NSMutableString alloc] init];
    }
    return self;
}

- (void)dealloc {
    self.instagram = nil;
    self.dataSource = nil;
    self.instagramRequestURLs = nil;
    self.urlString = nil;
    self.defaults = nil;
    self.appDelegate = nil;
    self.instagram = nil;
    self.userName = nil;
    self.instagramImages = nil;
    self.nextMaxId = nil;
    self.userId = nil;
}

#pragma mark -
#pragma mark - Instagram
- (void)picturesForUser:(NSString *)userName completion:(void (^)(void))completionBlock {
    self.userName = userName;
    [self instagramUserInfo:self.userName];
    self.completionBlock = completionBlock;
}
- (void)picturesForUserId:(NSNumber *)userId completion:(void (^)(void))completionBlock {
    self.userId = userId;
    self.completionBlock = completionBlock;
    [self recentPicturesListForUser];
}

- (void)instagramUserInfo:(NSString*)userName {
    //User info
    
//    https://api.instagram.com/v1/users/search?q=jack&access_token=ACCESS-TOKEN
    NSString *urlString = [NSString stringWithFormat:@"%@%@?q=%@", instagramUser, instagramUserSearch, userName];
//We use instagramRequestURLs for getting the proper Instagram data handler method's name.
//Because all the Instagram info the only method request:didLoad: delivers.
    [self.instagramRequestURLs setObject:urlString forKey:userKey];
    [self startConnectWithURLString:urlString];
}

- (void)recentPicturesListForUser {
// https://api.instagram.com/v1/users/3/media/recent/?access_token=ACCESS-TOKEN
    //recent pictures list
    NSMutableString *urlString = [[NSMutableString alloc] init];
    [urlString setString:[NSString stringWithFormat:@"%@/%@/media/recent/?count=%ld", instagramUser, self.userId, (long)maxAmountPerQuery]];
    if (self.nextMaxId.length > 0) {
        [urlString appendFormat:@"&max_id=%@",self.nextMaxId];
    }
    
    [self.instagramRequestURLs setObject:urlString forKey:picturesKey];
    [self startConnectWithURLString:urlString];
}

- (void)startConnectWithURLString:(NSString*)urlString {
    // here we can use accessToken received on previous login
    self.instagram.accessToken = [self loadDefaultValueForKey:instagramTokenKey];
    if ([self.instagram isSessionValid]) {
        //OK, we can get info
        [self makeRequestWihtUrlString:urlString];
    } else {
        //needs authorization
        self.urlString = nil;
        self.urlString = urlString;
        [self.instagram authorize:[NSArray arrayWithObjects:@"comments", @"likes", nil]];
    }
}

- (void)makeRequestWihtUrlString:(NSString*)urlString {
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:urlString, @"method", nil];
    [self.appDelegate.instagram requestWithParams:params delegate:self];
}


#pragma - IGSessionDelegate

-(void)igDidLogin {
    // here we can store accessToken
    [self saveDefaultValue:self.appDelegate.instagram.accessToken ForKey:instagramTokenKey];
    
    [self makeRequestWihtUrlString:self.urlString];
}

-(void)igDidNotLogin:(BOOL)cancelled {
    DLog(@"Instagram did not login");
    NSString* message = nil;
    if (cancelled) {
        message = @"Access cancelled!";
    } else {
        message = @"Access denied!";
    }
    [self showAlertWithMessage:message];

}

-(void)igDidLogout {
    // remove the accessToken
    [self deleteDefaultValueForKey:instagramTokenKey];
}

-(void)igSessionInvalidated {
    DLog(@"Instagram session was invalidated");
    [self deleteDefaultValueForKey:instagramTokenKey];
    [self.instagram authorize:[NSArray arrayWithObjects:@"comments", @"likes", nil]];
}

#pragma mark - IGRequestDelegate

- (void)request:(IGRequest *)request didFailWithError:(NSError *)error {
    DLog(@"Instagram did fail: %@", error);
    [self showAlertWithMessage:[error localizedDescription]];
    [self returnToSender];
}

//This method returns all the Instagram data
- (void)request:(IGRequest *)request didLoad:(id)result {
//For data with pagination
    [self nextMaxId:request.responseText];
//Searching the key, for object which is the same as the URL from the request.
//This key will be the handler method's name.
    NSSet *keys = [self.instagramRequestURLs keysOfEntriesPassingTest:^(id key, id obj, BOOL *stop){
        NSRange range = [request.url rangeOfString:(NSString*)obj];
        if (range.location != NSNotFound) {
            *stop = YES;
            return YES;
        }
        else {
            return NO;
        }
    }];
//    DLog(@"keys: %@", keys);
    if ( [keys count] > 0) {
//Handler of the Instagram data invoker
        [self performCustomSelector:[NSString stringWithFormat:@"%@:", [[NSArray arrayWithArray:[keys allObjects]] lastObject]] withObject:result];
    }
}

- (void)nextMaxId:(NSData*)data {
    NSError *error;
    NSDictionary *inputDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSDictionary *pagination = inputDictionary[@"pagination"];
//    DLog(@"next_max_id = %@", pagination[@"next_max_id"]);
    if (pagination) {
        if (pagination[@"next_max_id"]) {
            [self.nextMaxId setString:pagination[@"next_max_id"]];
        }
        else {
            [self.nextMaxId setString:@""];
        }
    }
}

- (void)performCustomSelector:(NSString*)selectorName withObject:(NSDictionary*)object{
    SEL selector = NSSelectorFromString(selectorName);
//Handler of the Instagram data invoking.
    if ([self respondsToSelector:selector]) {
#       pragma clang diagnostic push
#       pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self performSelector:selector withObject:object];
#       pragma clang diagnostic pop
    }
}
//Handler of the Instagram User info
- (void)userInfo:(NSDictionary*)info {
    NSArray* data = info[@"data"];
    if ([data count] == 0) {
        [self showAlertWithMessage:NSLocalizedString(@"NotFound", nil)];
        [self returnToSender];
    }
    else if ([data count] == 1) {//the only user
        NSDictionary* userInfo = data[0];
        self.userId = userInfo[@"id"];
        [self recentPicturesListForUser];
    } else {
        //return list of users
        [self composeUsersListFromData:data];
        [self returnToSender];
    }
}
//Handler of the Instagram User media info
- (void)picturesInfo:(NSDictionary*)info {
    if (!self.completionBlock) {
        return;
    }
    
    NSArray *data = info[@"data"];
    if ([data count] == 0) { //no data
        [self showAlertWithMessage:NSLocalizedString(@"NoData", nil)];
//Return back to TZMainVC
        [self returnToSender];
    }
    else {
//Add media info to the temporary storage
        [self.instagramImages addObjectsFromArray:[self composeInstagramObjectsFromData:data]];
        [self checkPicturesInfo];
    }
}
//To present media or to get more data.
- (void)checkPicturesInfo {
    if ([self.instagramImages count] >= maxAmountPerUser ||
        (self.nextMaxId.length == 0 && [self.instagramImages count] > 0)) {
        //present images
        [self prepareDataToPresent];
    } else {
        //proceed to get images
        [self recentPicturesListForUser];
    }
}

- (void)prepareDataToPresent {
    //sorting
    [self.instagramImages sortUsingDescriptors:
     @[[NSSortDescriptor sortDescriptorWithKey:@"likesCount" ascending:NO]]];
    
    //taking top pictures
    NSRange range = [self.instagramImages count] > topImagesAmount ? NSMakeRange(0, topImagesAmount) : NSMakeRange(0, [self.instagramImages count]);
    self.dataSource.dataInstagram = nil;
    self.dataSource.dataInstagram = [self.instagramImages subarrayWithRange:range];
    [self returnToSender];
    //clean
    [self.instagramImages removeAllObjects];
    [self.instagramRequestURLs removeAllObjects];
    [self.nextMaxId setString:@""];
}

#pragma mark -  Users list compose
- (void)composeUsersListFromData:(NSArray*)data {
    NSMutableArray *usersArray = [[NSMutableArray alloc] init];
    for (NSDictionary* item in data) {
        UserObject *userObject = [[UserObject alloc] init];
        userObject.userName = item[@"username"];
        userObject.userId = item[@"id"];
        [usersArray addObject:userObject];
    }
    self.dataSource.dataAmbiguousUsers = [NSArray arrayWithArray:usersArray];
}

#pragma mark - Instagram Object compose
- (NSArray*)composeInstagramObjectsFromData:(NSArray*)data {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for (NSDictionary* item in data) {
        InstagramObject *igObject = [self composeInstagramObject:item];
        if (igObject) {
            [result addObject:igObject];
        }
    }
    return result;
}

- (InstagramObject*)composeInstagramObject:(NSDictionary*)inputDictionary {
    InstagramObject *instagramObject = [[InstagramObject alloc] init];
    if (inputDictionary[@"images"]) {
        NSString *type = inputDictionary[@"type"];
        if (![type isEqualToString:@"image"]) {//use image only
            return nil;
        }
        NSDictionary *images = inputDictionary[@"images"];
        instagramObject.imageLowResolutionURL = images[@"low_resolution"][@"url"];
        instagramObject.imageStandardResolutionURL = images[@"standard_resolution"][@"url"];
        instagramObject.imageThumbnailURL = images[@"thumbnail"][@"url"];
        
        NSDictionary *likes = inputDictionary[@"likes"];
        instagramObject.likesCount = likes[@"count"];
    }

    return instagramObject;
}


#pragma mark - Internal
-(NSString*)loadDefaultValueForKey:(NSString*)key {
    return (NSString*)[[NSUserDefaults standardUserDefaults] objectForKey:key];
}

-(void)saveDefaultValue:(NSString*)value ForKey:(NSString*)key {
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)deleteDefaultValueForKey:(NSString*)key {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
}

- (void)showAlertWithMessage:(NSString *)message{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}

- (void)returnToSender {
    self.completionBlock();
}

@end
