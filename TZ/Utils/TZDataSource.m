

#import "TZDataSource.h"
static id sharedInstance;

@implementation TZDataSource

- (id) init {

    self = [super init];
    if(self){

    }
    return self;
}


+ (id) sharedInstance {
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        if (sharedInstance == nil) {
            sharedInstance = [[self alloc] init];
        }
    });
    
    return sharedInstance;
}


+ (id) copyWithZone: (NSZone *) zone {
    return self;
}


+ (id) allocWithZone: (NSZone *) zone {
    if (sharedInstance != nil) {
        return sharedInstance ;
    }
    else {
        return [super allocWithZone: zone];
    }
}

/*
- (id) retain {
    return self;
}


- (oneway void) release {
    
}

- (id) autorelease {
    return self;
}

- (NSUInteger) retainCount {
    return NSUIntegerMax;
}

- (void) dealloc {
    [super dealloc];
}
*/


@end
