//
//  Constants.h
//  TZ
//
//  Created by Alexey Yakushev on 4/14/14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#pragma mark - Instagram app
static NSString* const instagramClientId = @"319e362770604ba298c182d1e28e6585";
//images per query
static NSInteger const maxAmountPerQuery = 100;//in reality 33 only
//images per user
static NSInteger const maxAmountPerUser = 20000;

#pragma mark - Top pictures amount
static NSInteger const topImagesAmount = 1200;

#pragma mark - Social endpoints
#pragma mark - Instagram
static NSString* const instagramUser = @"users";
static NSString* const instagramUserSearch = @"/search";


#pragma mark - Instagram requests keys
static NSString* const userKey = @"userInfo";
static NSString* const picturesKey = @"picturesInfo";

#pragma mark - Tokens keys names
static NSString* const instagramTokenKey = @"accessTokenInstagram";

#pragma mark - Instagram cell
static NSInteger const spareInstgramCellWidth = 100;