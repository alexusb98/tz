//
//  TZPresenterVC.h
//  TZ
//
//  Created by Alexey Yakushev on 4/14/14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TZBaseVC.h"

@interface TZPresenterVC : TZBaseVC <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *printButton;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

- (IBAction)closeButtonClick:(id)sender;
- (IBAction)printButtonClick:(id)sender;
@end
