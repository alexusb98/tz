//
//  TZMainVC.m
//  TZ
//
//  Created by Alexey Yakushev on 4/13/14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import "TZMainVC.h"
#import "TZSocialService.h"
#import "TZPresenterVC.h"
#import "UserObject.h"

@interface TZMainVC ()
@property (strong, nonatomic) TZSocialService *socialService;
@property (strong, nonatomic) TZPresenterVC *presenterVC;
@property (assign, nonatomic) BOOL keyboardIsShown;
@property (strong, nonatomic) NSNotificationCenter *notificationCenter;
@end

@implementation TZMainVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.socialService = [[TZSocialService alloc] init];
        self.presenterVC = [[TZPresenterVC alloc] initWithNibName:@"TZPresenterVC" bundle:nil];
        self.notificationCenter = [NSNotificationCenter defaultCenter];
        [self registerKeyboardNotifications];
        self.keyboardIsShown = NO;

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.textField.delegate = self;
    //activity view
    self.activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [self.view addSubview:self.activityView];
    //tableView
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self hideTableView];
}

- (void)dealloc {
    self.toolBar = nil;
    self.textField = nil;
    self.socialService = nil;
    self.presenterVC = nil;
    self.tableView = nil;
    [self.notificationCenter removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [self.notificationCenter removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    self.notificationCenter = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Internal
- (void)buttonsText {
    [self.toolBar.items[0] setTitle:NSLocalizedString(@"MakeCollage", nil)];
}

- (void)hideKeyboard {
    [self.view endEditing:YES];
}

- (void)textFieldText:(NSString*)text {
    self.textField.text = text;
}

#pragma mark - Actions
- (IBAction)collageButtonClick:(id)sender {
    if ([self.textField.text length] == 0) {
        return;
    }
    [self hideKeyboard];
    [self picturesForUser:self.textField.text];
}

#pragma mark - TextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    [self picturesForUser:self.textField.text];
    [self collageButtonClick:self];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self hideTableView];
}

#pragma mark - SocialService call
- (void)picturesForUser:(NSString*)userName {
    [self.activityView startAnimating];
    //Pictures for user
    [self.socialService picturesForUser:userName completion:^void{
        [self.activityView stopAnimating];
        [self completionBlockHandler];
    }];
}

- (void)picturesForUserId:(NSNumber*)userId {
    [self.activityView startAnimating];
    //Pictures for user
    [self.socialService picturesForUserId:userId completion:^void{
        [self.activityView stopAnimating];
        [self completionBlockHandler];
    }];
}

- (void)completionBlockHandler {
    if ([self.dataSource.dataInstagram count] > 0) {
        [self presentViewController:self.presenterVC animated:YES completion:nil];
    }
    else if([self.dataSource.dataAmbiguousUsers count] > 0 ){
        [self showTableView];
    }
    
    [self textFieldText:@""];
}

#pragma mark - TableView
- (void)hideTableView {
    [self.tableView setHidden:YES];
    self.dataSource.dataAmbiguousUsers = nil;
    [self.tableView reloadData];
}

- (void)showTableView {
    [self.tableView setHidden:NO];
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataSource.dataAmbiguousUsers count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return NSLocalizedString(@"TableTitle", nil);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UserObject *userObject = self.dataSource.dataAmbiguousUsers[indexPath.row];
    [self picturesForUserId:userObject.userId];
    [self hideTableView];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = nil;
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    UserObject *userObject = self.dataSource.dataAmbiguousUsers[indexPath.row];
    cell.textLabel.text = userObject.userName;
    
    return cell;
}

#pragma mark - Keyboard
- (void)registerKeyboardNotifications {
    [self.notificationCenter addObserver:self selector:@selector(keyboardIsShown:) name:UIKeyboardDidShowNotification object:nil];
    [self.notificationCenter addObserver:self selector:@selector(keyboardIsAbsent:) name:UIKeyboardDidHideNotification object:nil];
}

- (void)keyboardIsShown:(NSNotification *)notification {
    self.keyboardIsShown = YES;
//    NSDictionary *info  = notification.userInfo;
//    NSValue      *value = info[UIKeyboardFrameEndUserInfoKey];
//    
//    CGRect rawFrame      = [value CGRectValue];
//    CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
//    CGFloat keyboardHeight = CGRectGetHeight(keyboardFrame);
//    DLog(@"keyboardHeight: %f", keyboardHeight);
}

- (void)keyboardIsAbsent:(NSNotification *)notification {
    self.keyboardIsShown = NO;
}

#pragma mark - Keyboard dissmiss
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.keyboardIsShown != NO) {
        [self.view endEditing:YES];
    }
}


@end
