//
//  TZPresenterVC.m
//  TZ
//
//  Created by Alexey Yakushev on 4/14/14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import "TZPresenterVC.h"
#import "TZInstagramCell.h"
#import "InstagramObject.h"
#import "Constants.h"
#import "AsyncImageView.h"

@interface TZPresenterVC ()
@property (assign, nonatomic) CGSize instgramCellSize;
@property (strong, nonatomic) UICollectionViewFlowLayout *flowLayout;
@property (strong, nonatomic) UICollectionViewLayout *layout;
@property (strong, nonatomic) NSMutableArray *selectedPicturesArray;
@property (strong, nonatomic) UIPrintInteractionController *printVC;
@end

@implementation TZPresenterVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.flowLayout = [[UICollectionViewFlowLayout alloc] init];
        self.layout = [[UICollectionViewLayout alloc] init];
        self.selectedPicturesArray = [[NSMutableArray alloc] init];
        self.printVC = [UIPrintInteractionController sharedPrintController];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.collectionView setCollectionViewLayout:self.flowLayout];
//    [self.collectionView setCollectionViewLayout:self.layout];
    
    //activity view
    [self.view addSubview:self.activityView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self prepareCollectionView];
    [self printButtonAvailability];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    self.toolBar = nil;
    self.printButton = nil;
    self.collectionView = nil;
    self.flowLayout = nil;
    self.layout = nil;
    self.selectedPicturesArray = nil;
    self.printVC = nil;
}

#pragma mark - Internal
- (void)buttonsText {
    [self.toolBar.items[0] setTitle:NSLocalizedString(@"Close", nil)];
    [self.toolBar.items[2] setTitle:NSLocalizedString(@"Print", nil)];
}

- (void)printButtonAvailability {
    if ([[self selectedIndexPathsArray] count] == 0) {
        self.printButton.enabled = NO;
    } else {
        self.printButton.enabled = YES;
    }
    
}

- (void)prepareCollectionView {
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.allowsSelection = YES;
    self.collectionView.allowsMultipleSelection = YES;
    
    // Register the colleciton cell
    [self.collectionView registerNib:[UINib nibWithNibName:@"TZInstagramCell" bundle:nil] forCellWithReuseIdentifier:@"TZInstagramCell"];
    //FlowLayout
    [self prepareFlowLayout];
    //Orientation
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait) {
        [self handlePortraitOrientation];
    } else {
        [self handleLandscapeOrientation];
    }

    [self.collectionView reloadData];
    
}

- (void)prepareFlowLayout {
    //instgramCellSize
    [self calcInstgramCellSize];
    
    self.flowLayout.itemSize = self.instgramCellSize.width > 0 ? self.instgramCellSize : CGSizeMake(spareInstgramCellWidth, spareInstgramCellWidth);
    
    self.flowLayout.minimumLineSpacing = 0;
    self.flowLayout.minimumInteritemSpacing = 0;
    
    NSInteger numberOfCells = self.view.frame.size.width / self.flowLayout.itemSize.width;
    NSInteger edgeInsets = (self.view.frame.size.width - (numberOfCells * self.flowLayout.itemSize.width)) / (numberOfCells + 1);
    
    self.flowLayout.sectionInset = UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets);
//    self.flowLayout.sectionInset = UIEdgeInsetsMake(10,10,10,10);
}

- (void)calcInstgramCellSize {
    CGRect screenFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat length = 0;
    if (self.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        length = CGRectGetWidth(screenFrame) /4.;
    }
    else {
        length = CGRectGetWidth(screenFrame) /6.;
    }
    self.instgramCellSize = CGSizeMake(length, length);

}

- (void)calcSectionInset {
    self.flowLayout.sectionInset = UIEdgeInsetsMake(10,10,10,10);
}

- (NSString*)urlForCellAtIndexPath:(NSIndexPath*)indexPath {
    NSString *result;
    InstagramObject *instagram = self.dataSource.dataInstagram[indexPath.row];
    result = instagram.imageThumbnailURL;
    
    return result;
}

- (NSArray*)selectedIndexPathsArray {
    return [self.collectionView indexPathsForSelectedItems];
}

- (void)deselectCellAtIndexPath:(NSIndexPath*)indexPath {
    [self handleDeselectionForIndexPath:indexPath];
    [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

#pragma mark - Actions
- (IBAction)closeButtonClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        self.dataSource.dataInstagram = nil;
        self.dataSource.dataAmbiguousUsers = nil;
    }];
}

- (IBAction)printButtonClick:(id)sender {
    if ([[self selectedIndexPathsArray] count] > 1) {
        //New collage
        [self makeNewCollage];
    } else {
        if ([UIPrintInteractionController isPrintingAvailable] != NO) {
            [self.activityView startAnimating];
            [self loadFullSizeSelectedImage];
        } else {
            [self handlePrintingProcessWithMessageText:NSLocalizedString(@"NoPrint", nil)];
        }
        
    }
}

#pragma mark - UICollectionView Datasource
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
     return [self.dataSource.dataInstagram count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TZInstagramCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"TZInstagramCell" forIndexPath:indexPath];
    cell.imageView.alpha = 1;
    cell.imageView.image = nil;
    
    //cancel loading previous image for cell
    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.imageView];
    cell.imageView.imageURL = [NSURL URLWithString:[self urlForCellAtIndexPath:indexPath]];
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self handleSelectionForIndexPath:indexPath];
    [collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];

}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self deselectCellAtIndexPath:indexPath];
}

#pragma mark - Selection handler
- (void)handleSelectionForIndexPath:(NSIndexPath*)indexPath {
    TZInstagramCell *cell = (TZInstagramCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
//    DLog(@"selected count:%d", [self selectedIndexPathsArray].count);
    cell.imageView.alpha = 0.8;

    [self checkSelectedCellsCount];
}

- (void)handleDeselectionForIndexPath:(NSIndexPath*)indexPath {
    TZInstagramCell *cell = (TZInstagramCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
//    DLog(@"selected count:%d", [self selectedIndexPathsArray].count);
    cell.imageView.alpha = 1;

    [self checkSelectedCellsCount];
}

- (void)checkSelectedCellsCount {
    if ([[self selectedIndexPathsArray] count] > 1) {
        [self.toolBar.items[2] setTitle:NSLocalizedString(@"MakeNewCollage", nil)];
    } else {
        [self.toolBar.items[2] setTitle:NSLocalizedString(@"Print", nil)];
    }
    [self printButtonAvailability];
}

#pragma mark - New collage
- (void)makeNewCollage {

    [self.selectedPicturesArray removeAllObjects];
    NSArray *selectedIndexPaths = [self selectedIndexPathsArray];
    for (NSIndexPath* indexPath in selectedIndexPaths) {
        [self.selectedPicturesArray addObject:self.dataSource.dataInstagram[indexPath.row]];
        [self deselectCellAtIndexPath:indexPath];
    }
    [self printButtonAvailability];
    self.dataSource.dataInstagram = nil;
    self.dataSource.dataInstagram = [NSArray arrayWithArray:self.selectedPicturesArray];
    [self.selectedPicturesArray removeAllObjects];
    self.collectionView.allowsMultipleSelection = NO;
    [self.collectionView reloadData];
    [self buttonsText];
}

#pragma mark - Rotation
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait ||
        self.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {//became portrait
        [self handlePortraitOrientation];
    } else {
        [self handleLandscapeOrientation];
    }
    [self.collectionView.collectionViewLayout invalidateLayout];
//    [self.collectionView reloadData];
}

- (void)handlePortraitOrientation {
    self.flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    //instgramCellSize
//    [self calcInstgramCellSize];
}

- (void)handleLandscapeOrientation {
    self.flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    //instgramCellSize
//    [self calcInstgramCellSize];
}

#pragma mark - Print
- (void)loadFullSizeSelectedImage {
    NSIndexPath *selectedIndexPath = [[self selectedIndexPathsArray] lastObject];
    NSString *stringURL = [self.dataSource.dataInstagram[selectedIndexPath.row] imageStandardResolutionURL];
    NSURL *imageURL = [NSURL URLWithString:stringURL];
    [[AsyncImageLoader sharedLoader] loadImageWithURL:imageURL target:self action:@selector(printImage:)];
}

- (void)printImage:(UIImage*)image{
    //Print possibility check
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(image)];
    if ([UIPrintInteractionController canPrintData:imageData] == NO) {
        [self handlePrintingProcessWithMessageText:NSLocalizedString(@"NoImagePrint", nil)];
        return;
    }
    self.printVC.printingItem = image;
    
    
    UIPrintInteractionCompletionHandler completionHandler = [self completionHandler];

    //printInfo
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputPhoto;
    printInfo.jobName = image.description;
    if(!self.printVC.printingItem && image.size.width > image.size.height) {
        printInfo.orientation = UIPrintInfoOrientationLandscape;
    };
    self.printVC.printInfo = printInfo;
    
    [self.activityView stopAnimating];
    [self.printVC presentAnimated:YES completionHandler:completionHandler];
}

- (void (^)(UIPrintInteractionController*, BOOL, NSError*))completionHandler {
    
    void (^completionHandler)(UIPrintInteractionController*, BOOL, NSError*) =
    ^void(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
        [self handlePrintingProcessWithMessageText:nil];
        self.printVC.printingItem = nil;
        if (!completed && error) {
            NSLog(@"FAILED! due to error in domain %@ with error code %ld", error.domain, (long)error.code);
            [self showAlertWithMessage:NSLocalizedString(@"PrintError", nil)];
        }
        
    };
    return [completionHandler copy];
}

- (void)handlePrintingProcessWithMessageText:(NSString*)messageText {
    NSIndexPath *indexPath = [[self selectedIndexPathsArray] lastObject];
    [self deselectCellAtIndexPath:indexPath];
    if (messageText) {
        [self showAlertWithMessage:messageText];
    }
    
}

@end
