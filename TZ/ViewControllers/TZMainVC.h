//
//  TZMainVC.h
//  TZ
//
//  Created by Alexey Yakushev on 4/13/14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TZBaseVC.h"

@interface TZMainVC : TZBaseVC <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


- (IBAction)collageButtonClick:(id)sender;

@end
