//
//  TZBaseVC.h
//  TZ
//
//  Created by Alexey Yakushev on 4/14/14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TZDataSource.h"

@interface TZBaseVC : UIViewController
@property (strong, nonatomic) TZDataSource *dataSource;
@property (nonatomic, strong) UIActivityIndicatorView *activityView;
@property (nonatomic, strong) UIDevice *currentDevice;



- (void)buttonsText;
- (void)showAlertWithMessage:(NSString *)message;
@end
