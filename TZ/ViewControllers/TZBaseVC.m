//
//  TZBaseVC.m
//  TZ
//
//  Created by Alexey Yakushev on 4/14/14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import "TZBaseVC.h"


@interface TZBaseVC ()


@end

@implementation TZBaseVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.dataSource = [TZDataSource sharedInstance];
        [self initActivityView];
        self.currentDevice = [UIDevice currentDevice];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
     //Buttons title
    [self buttonsText];
    self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    self.dataSource = nil;
    self.activityView = nil;
    self.currentDevice = nil;
}

#pragma mark - Internal
- (void)buttonsText {
}

- (void)showAlertWithMessage:(NSString *)message{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}

- (void)initActivityView{
    self.activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.activityView.hidesWhenStopped = YES;
    self.activityView.center = CGPointMake([[UIScreen mainScreen] applicationFrame].size.width / 2.0f, [[UIScreen mainScreen] applicationFrame].size.height / 2.0f);
    self.activityView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
}


@end
