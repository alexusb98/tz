//
//  TZInstagramCell.m
//  TZ
//
//  Created by Alexey Yakushev on 4/14/14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import "TZInstagramCell.h"

@interface TZInstagramCell ()

@end

@implementation TZInstagramCell

- (void)dealloc {
    self.imageView = nil;
}

@end
