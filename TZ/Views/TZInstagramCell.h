//
//  TZInstagramCell.h
//  TZ
//
//  Created by Alexey Yakushev on 4/14/14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface TZInstagramCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet AsyncImageView *imageView;

@end
