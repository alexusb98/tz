//
//  main.m
//  TZ
//
//  Created by Alexey Yakushev on 4/9/14.
//  Copyright (c) 2014 Alexey Yakushev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TZAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TZAppDelegate class]));
    }
}
